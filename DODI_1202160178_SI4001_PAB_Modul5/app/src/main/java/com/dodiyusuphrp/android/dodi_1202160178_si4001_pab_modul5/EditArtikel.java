package com.dodiyusuphrp.android.dodi_1202160178_si4001_pab_modul5;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

import java.util.List;

public class EditArtikel extends AppCompatActivity {

    private ArtikelDataSource db;
    private List<Artikel> values;
    private Artikel artikel = new Artikel();
    private EditText edJudul, edAuthor, edDate, edDesc;
    Long id;
    String judul, author, date, desc;
    private CoordinatorLayout coord_edit;
    boolean theme;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        SharedPreferences preferences = getSharedPreferences("prefs", MODE_PRIVATE);
        boolean theme = preferences.getBoolean("dark_theme", false);
        boolean font = preferences.getBoolean("font_large", false);
        if (theme && font) {
            setTheme(R.style.AppTheme_Dark_FontLarge);
        } else if (theme) {
            setTheme(R.style.AppTheme_Dark_FontNormal);
        } else if (font) {
            setTheme(R.style.AppTheme_FontLarge);
        }
        setContentView(R.layout.activity_edit_artikel);
        db = new ArtikelDataSource(this);
        db.open();
        initUI();


    }

    private void initUI() {
        Bundle i = getIntent().getExtras();
        if (i != null) {
            id = i.getLong("id");
            judul = i.getString("judul");
            author = i.getString("author");
            date = i.getString("date");
            desc = i.getString("desc");
        }
        edJudul = findViewById(R.id.edit_edittext_judul);
        edAuthor = findViewById(R.id.edit_edittext_author);
        edDate = findViewById(R.id.edit_edittext_tanggal);
        edDesc = findViewById(R.id.edit_edittext_desc);
        coord_edit = findViewById(R.id.edit_coordinator);
        edJudul.setText(judul);
        edDesc.setText(desc);
        edDate.setText(date);
        edAuthor.setText(author);
    }

    public void updateArtikel(View view) {
        if (TextUtils.isEmpty(edJudul.getText())) {
            Snackbar.make(coord_edit, "Judul wajib diisi!", Snackbar.LENGTH_LONG)
                    .show();
            edJudul.setError("Judul wajib diisi!");
            return;
        }
        if (TextUtils.isEmpty(edAuthor.getText())) {
            Snackbar.make(coord_edit, "Nama penulis wajib diisi!", Snackbar.LENGTH_LONG)
                    .show();
            edAuthor.setError("Nama penulis wajib diisi!");
            return;
        }
        if (TextUtils.isEmpty(edDate.getText())) {
            Snackbar.make(coord_edit, "Tanggal tidak benar!", Snackbar.LENGTH_LONG)
                    .show();
            edDate.setError("Tanggal tidak benar!");
            return;
        }
        if (TextUtils.isEmpty(edDesc.getText())) {
            Snackbar.make(coord_edit, "Deskripsi wajib diisi!", Snackbar.LENGTH_LONG)
                    .show();
            edJudul.setError("Deskripsi wajib diisi!");
            return;
        }
        Log.d("EDIT", edJudul.getText().toString());
        artikel.setId(id);
        artikel.setTitle(edJudul.getText().toString());
        artikel.setAuthor(edAuthor.getText().toString());
        artikel.setCreated(edDate.getText().toString());
        artikel.setDesc(edDesc.getText().toString());
        db.updateArtikelById(artikel);
        Intent intent = new Intent(EditArtikel.this, ViewArtikel.class);
        startActivity(intent);
        db.close();
        Log.d("EDITARTICLE", artikel.getId().toString());
        finish();

    }
}