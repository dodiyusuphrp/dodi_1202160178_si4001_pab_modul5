package com.dodiyusuphrp.android.dodi_1202160178_si4001_pab_modul5;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity {
    CoordinatorLayout coord_main;
    boolean theme;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        SharedPreferences preferences = getSharedPreferences("prefs", MODE_PRIVATE);
        theme = preferences.getBoolean("dark_theme", false);
        boolean font = preferences.getBoolean("font_large", false);
        if (theme && font) {
            setTheme(R.style.AppTheme_Dark_FontLarge);
        } else if (theme) {
            setTheme(R.style.AppTheme_Dark_FontNormal);
        } else if (font) {
            setTheme(R.style.AppTheme_FontLarge);
        }
        setContentView(R.layout.activity_main);
        initUI();
    }
    private void initUI() {
        coord_main = findViewById(R.id.main_coordinator);
    }

    public void viewArtikel(View view) {
        Intent intent = new Intent (MainActivity.this, ViewArtikel.class);
        startActivity(intent);
    }

    public void buatArtikel(View view) {
        Intent intent = new Intent(MainActivity.this, BuatArtikelActivity.class);
        startActivity(intent);
    }

    public void settings(View view) {
        Intent intent = new Intent(MainActivity.this, SettingsActivity.class);
        startActivity(intent);
    }
}