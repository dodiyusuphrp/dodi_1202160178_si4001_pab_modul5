package com.dodiyusuphrp.android.dodi_1202160178_si4001_pab_modul5;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;

public class ArtikelDataSource  {

    private SQLiteDatabase database;
    private ArtikelDB artikelDB;
    private String[] allColumns = {ArtikelDB.COLUMN_ID, ArtikelDB.JUDUL_ARTIKEL,
            ArtikelDB.AUTHOR_ARTIKEL, ArtikelDB.DESC_ARTIKEL, ArtikelDB.CREATED_DATE};

    public ArtikelDataSource(Context context) {
        artikelDB = new ArtikelDB(context);
    }

    public void open() throws SQLException {
        database = artikelDB.getWritableDatabase();
    }

    public void close() throws SQLException {
        artikelDB.close();
    }

    public Artikel createArtikel(String judul, String author, String desc, String date) {
        ContentValues values = new ContentValues();
        values.put(ArtikelDB.JUDUL_ARTIKEL, judul);
        values.put(ArtikelDB.AUTHOR_ARTIKEL, author);
        values.put(ArtikelDB.DESC_ARTIKEL, desc);
        values.put(ArtikelDB.CREATED_DATE, date);

        long insertId = database.insert(ArtikelDB.TABLE_NAME, null, values);
        Cursor cursor = database.query(ArtikelDB.TABLE_NAME, allColumns, ArtikelDB.COLUMN_ID
                + " = " +insertId, null, null, null, null);
        cursor.moveToFirst();
        Artikel newArtikel = cursorToArtikel(cursor);
        cursor.close();
        return newArtikel;
    }

    private Artikel cursorToArtikel(Cursor cursor) {
        Artikel artikel = new Artikel();
        Log.v("TESTER", "LONG"+cursor.getLong(0));
        Log.v("TESTER", "setOthers"+cursor.getString(1)+", "+cursor.getString(2));
        artikel.setId(cursor.getLong(0));
        artikel.setTitle(cursor.getString(1));
        artikel.setAuthor(cursor.getString(2));
        artikel.setDesc(cursor.getString(3));
        artikel.setCreated(cursor.getString(4));
        return artikel;
    }

    public ArrayList<Artikel> readArtikel() {
        ArrayList<Artikel> alArtikel = new ArrayList<Artikel>();
        Cursor cursor = database.query(artikelDB.TABLE_NAME, allColumns, null, null, null, null, null);
        cursor.moveToFirst();

        while (!cursor.isAfterLast()) {
            Artikel artikel = cursorToArtikel(cursor);
            alArtikel.add(artikel);
            cursor.moveToNext();
        }
        cursor.close();
        return alArtikel;
    }

    public Artikel findArtikelById(long id) {
        Artikel artikel = new Artikel();
        Cursor cursor = database.query(ArtikelDB.TABLE_NAME, allColumns, "_id="+id, null, null, null, null);
        cursor.moveToFirst();
        artikel = cursorToArtikel(cursor);
        cursor.close();
        return artikel;
    }

    public void updateArtikelById(Artikel a) {
        String mFilter = "_id="+a.getId();
        ContentValues args = new ContentValues();
        args.put(ArtikelDB.JUDUL_ARTIKEL, a.getTitle());
        args.put(ArtikelDB.AUTHOR_ARTIKEL, a.getAuthor());
        args.put(ArtikelDB.DESC_ARTIKEL, a.getDesc());
        args.put(ArtikelDB.CREATED_DATE, a.getCreated());
        database.update(ArtikelDB.TABLE_NAME, args, mFilter, null);
    }

    public void deleteArtikelById(long id) {
        String mFilter = "_id="+id;
        database.delete(ArtikelDB.TABLE_NAME, mFilter, null);
    }
    public void deleteAllArtikel() {
        database.delete(ArtikelDB.TABLE_NAME, "1", null);
        database.close();
    }
}

