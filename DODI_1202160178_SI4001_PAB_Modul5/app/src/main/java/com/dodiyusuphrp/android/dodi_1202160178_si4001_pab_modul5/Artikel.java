package com.dodiyusuphrp.android.dodi_1202160178_si4001_pab_modul5;

import android.support.annotation.NonNull;

public class Artikel {
    @NonNull
    private long id;
    private String title, author, desc, created;

    @NonNull
    public Long getId() {
        return id;
    }

    public void setId(@NonNull long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    public
    Artikel() {

    }
}



