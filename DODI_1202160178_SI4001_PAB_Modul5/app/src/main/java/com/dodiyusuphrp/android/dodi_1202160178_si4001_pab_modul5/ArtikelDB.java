package com.dodiyusuphrp.android.dodi_1202160178_si4001_pab_modul5;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class ArtikelDB extends SQLiteOpenHelper {

    public static final String TABLE_NAME = "artikel";
    public static final String COLUMN_ID = "_id";
    public static final String JUDUL_ARTIKEL = "judul_artikel";
    public static final String AUTHOR_ARTIKEL = "author_artikel";
    public static final String DESC_ARTIKEL = "desc_artikel";
    public static final String CREATED_DATE = "date_created";
    public static final String db_name ="artikelDB.db";
    public static final int db_version=1;
    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(db_create);
    }

    private static final String db_create = "create table "
            +TABLE_NAME+ "("
            +COLUMN_ID+" integer primary key autoincrement, "
            +JUDUL_ARTIKEL+" varchar(50) not null, "
            +DESC_ARTIKEL+" varchar(500) not null, "
            +AUTHOR_ARTIKEL+" varchar(50) not null, "
            +CREATED_DATE+" varchar(50) not null);";

    public ArtikelDB(Context context) {
        super(context, db_name, null, db_version);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.w(ArtikelDB.class.getName(), "Upgrading database from version "+oldVersion
                + " to "+newVersion+" which will destroy all data");
        db.execSQL("DROP TABLE IF EXISTS "+TABLE_NAME);
        onCreate(db);
    }
}

