package com.dodiyusuphrp.android.dodi_1202160178_si4001_pab_modul5;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import java.util.ArrayList;

public class ArtikelAdapter extends RecyclerView.Adapter<ArtikelAdapter.ViewHolder> {

    Context context;
    ArrayList<Artikel> artikelList;

    public void deleteData(int adapterPosition) {
        Artikel a = artikelList.get(adapterPosition);
        long id = a.getId();
        ArtikelDataSource db = new ArtikelDataSource(context);
        db.open();
        db.deleteArtikelById(id);
        artikelList.remove(adapterPosition);
        db.close();
        this.notifyItemRemoved(adapterPosition);
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        TextView tvJudul, tvTanggal, tvAuthor, tvIsi;
        ImageButton btnShare, btnEdit;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            tvJudul = itemView.findViewById(R.id.viewcard_txv_judul);
            tvTanggal = itemView.findViewById(R.id.viewcard_txv_date);
            tvAuthor = itemView.findViewById(R.id.viewcard_txv_nama);
            tvIsi = itemView.findViewById(R.id.viewcard_txv_content);
            btnShare = itemView.findViewById(R.id.viewcard_btn_share);
            btnEdit = itemView.findViewById(R.id.viewcard_btn_edit);
        }
    }

    public ArtikelAdapter(Context context, ArrayList<Artikel> artikelList) {
        this.context = context;
        this.artikelList = artikelList;
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @NonNull
    @Override
    public ArtikelAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.view_list_card, viewGroup, false);
        itemView.setElevation(6);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ArtikelAdapter.ViewHolder viewHolder, int i) {
        final Artikel artikel = artikelList.get(i);
        viewHolder.tvJudul.setText(artikel.getTitle());
        viewHolder.tvAuthor.setText(artikel.getAuthor());
        viewHolder.tvTanggal.setText(artikel.getCreated());
        viewHolder.tvIsi.setText(artikel.getDesc());
        viewHolder.btnShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_SEND);
                intent.setType("text/plain");
                String shareBodyText = artikel.getTitle()
                        + " oleh "+artikel.getAuthor()+"\n"
                        +artikel.getDesc()
                        +"\n\n. Dibagikan oleh aplikasi Voltpad (bukan Wattpad).";
                intent.putExtra(Intent.EXTRA_SUBJECT, artikel.getTitle());
                intent.putExtra(Intent.EXTRA_TEXT, shareBodyText);
                v.getContext().startActivity(Intent.createChooser(intent, "Choose sharing method"));
            }
        });
        viewHolder.btnEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(), EditArtikel.class);
                Bundle bundle = new Bundle();
                bundle.putLong("id", artikel.getId());
                bundle.putString("judul", artikel.getTitle());
                bundle.putString("author", artikel.getAuthor());
                bundle.putString("date", artikel.getCreated());
                bundle.putString("desc", artikel.getDesc());
                intent.putExtras(bundle);
                v.getContext().startActivity(intent);
            }
        });
        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(), DetailsActivity.class);
                Bundle bundle = new Bundle();
                bundle.putLong("id", artikel.getId());
                bundle.putString("judul", artikel.getTitle());
                bundle.putString("author", artikel.getAuthor());
                bundle.putString("date", artikel.getCreated());
                bundle.putString("desc", artikel.getDesc());
                intent.putExtras(bundle);
                v.getContext().startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        if (artikelList == null) return 0;
        return artikelList.size();
    }
}

